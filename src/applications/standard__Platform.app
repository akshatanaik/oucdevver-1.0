<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-Contact</defaultLandingTab>
    <tab>standard-Contact</tab>
    <tab>standard-Account</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Chatter</tab>
    <tab>standard-File</tab>
    <tab>ServiceRequestType__c</tab>
    <tab>Jurisdiction__c</tab>
    <tab>FlexNote__c</tab>
    <tab>FlexNoteQuestion__c</tab>
    <tab>ServiceRequestType_Jurisdiction__c</tab>
    <tab>ServiceRequestTypeFlexNote__c</tab>
    <tab>Service_Request_History__c</tab>
    <tab>Flex_Note_History__c</tab>
    <tab>Citizens_s_Portal</tab>
    <tab>OUC_Activity__c</tab>
    <tab>Decode_Object__c</tab>
    <tab>Agent_Console</tab>
    <tab>OUC_Activity_History__c</tab>
    <tab>Open311_API_Log__c</tab>
</CustomApplication>
