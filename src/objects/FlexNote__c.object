<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Stores FlexNotes for a Case Object.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Answer_Date_Time__c</fullName>
        <externalId>false</externalId>
        <label>Answer Date Time</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Answer__c</fullName>
        <externalId>false</externalId>
        <label>Answer</label>
        <length>131072</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>2</visibleLines>
    </fields>
    <fields>
        <fullName>Case__c</fullName>
        <externalId>false</externalId>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>FlexNotes</relationshipLabel>
        <relationshipName>FlexNotes</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>CodeDescription__c</fullName>
        <externalId>false</externalId>
        <label>Code Description</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>FlexNoteQuestionCode__c</fullName>
        <externalId>false</externalId>
        <formula>FlexNote_Question__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>FlexNoteQuestionCode</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>FlexNote_Question__c</fullName>
        <externalId>false</externalId>
        <label>FlexNote Question</label>
        <referenceTo>FlexNoteQuestion__c</referenceTo>
        <relationshipLabel>FlexNotes</relationshipLabel>
        <relationshipName>FlexNotes</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Is_Answer__c</fullName>
        <externalId>false</externalId>
        <label>Answer(to be removed)</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LegacyID__c</fullName>
        <externalId>true</externalId>
        <label>LegacyID</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Order__c</fullName>
        <externalId>false</externalId>
        <label>Order</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Question__c</fullName>
        <externalId>false</externalId>
        <label>Question(To be deleted)</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Questions__c</fullName>
        <description>This has been created instead of (Question__c)question for data type change</description>
        <externalId>false</externalId>
        <label>Question</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <label>Flex Note</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Case__c</columns>
        <columns>FlexNote_Question__c</columns>
        <columns>CodeDescription__c</columns>
        <columns>LegacyID__c</columns>
        <columns>Answer__c</columns>
        <columns>Questions__c</columns>
        <columns>OBJECT_ID</columns>
        <columns>Order__c</columns>
        <columns>CREATEDBY_USER.ALIAS</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>All1</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>FN-{00000000}</displayFormat>
        <label>Flex Note Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>FlexNotes</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
