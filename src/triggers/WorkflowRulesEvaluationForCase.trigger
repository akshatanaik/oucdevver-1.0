trigger WorkflowRulesEvaluationForCase  on Case(after insert,after update) {
    
    
   Set<Id> Ids=new Set<Id>();
   String sobjName='Case';
    
    
    //add all the accountids in the set
    for (Case a:trigger.new){
        Ids.add(a.Id);       
    }//for 

    
    //decide whether we need to start the process:- changing here for trigger chaining @sneha   
    Boolean evaluate = WorkflowRuleEvaluation.EvaluationIsRunning;
    
    
    //Send that list of created or updated account to the Rule Engine class for evaluation
    try{
        if (!evaluate){   
                if(Trigger.isUpdate)
                    WorkflowRuleEvaluation.oldListMap = Trigger.oldMap;    
                 WorkflowRuleEvaluation.startWorkflow(Ids,sobjName,Trigger.isInsert);       
        }   
        
        /*
        * This trigger creates a copy of a Case obj in CaseHistory
        * when a case is created or edited
        * Created by Akshata Naik
        * Created date: 17-09-2014 
        */
        
        Boolean isSnowBatchRunning = SnowAttributesFetchBatch.isSnowAttributeBatchRunning;
        
        system.debug('isSnowBatchRunning-->'+isSnowBatchRunning);
        List <String> caseids = new  List <String>();
        for (Case cse: trigger.new){
            string caseId =cse.Id;
            caseids.add(caseId);
        }
        
        if(!isSnowBatchRunning){
	        CloneHistoryObjects.cloneServiceRequestHistory(caseids);
        }
        else{
        	CloneHistoryObjects.startCloneServiceRequestHistory(caseids); // testing purpose added
        }
       
   }  
    //Do not Fail this trigger
   catch(Exception e){
       if(Test.isRunningTest()){ 
           System.debug('This is not working properly'); 
        }   
        else
           throw(e);     
   
   }
}