trigger ToUpdateFlexNoteCodeDescription on FlexNote__c (before insert, before update) {
    list <String> fqIds = new list <String>();
    list<FlexNote__c>fn=new list<FlexNote__c>();
    for (FlexNote__c a:trigger.new){
        fqIds.add(a.FlexNote_Question__c);
    }
    list <FlexNoteQuestion__c> fnList =[select id,name,AnswerValues__c ,QuestionAlias__c,Answer_Type__c,Questions__c,Required__c from FlexNoteQuestion__c where id in:fqIds];
    // comment 3010
    /*Map<Id, String> decodeStr = new Map<Id, String>();
    decodeStr =ServiceRequestsHelper.getQuestionCodeAnswerMap(fnList);
    
    for(FlexNote__c oAnswer : trigger.new){
       //oAnswer.CodeDescription__c=decodeStr.get(oAnswer.FlexNote_Question__c);
   
    }*/
   // added 3010
    ServiceRequestsHelper.startFlexNoteProcess(trigger.new,fnList);
}