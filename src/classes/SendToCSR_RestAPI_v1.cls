@RestResource(urlMapping='/v1/sendtocsr')
global  class SendToCSR_RestAPI_v1 {

		/*
	        POST updates from a external app
	        @Purpose :-
	      	  This is an internal enpoint that accepts a xml payload from 
			  PHP Soap service running inside the fire wall of trackster
			  to update a service request
	        
	       
	        Formats JSON
	        HTTP Method - POST
	        
	        
	        @Params:-
	        csrXML :- payload
	        
	        Sample Call:     
	        https://[API endpoint]/sendtocsr        
	    */
	    @HttpPost
	    global static String doPost(){
	    	RestRequest req = RestContext.request; 
        	RestResponse response = RestContext.response; 
	    	
	    	Dom.Document doc = new Dom.Document();
	        Dom.Xmlnode rootNode = doc.createRootElement('csreai:csreai-msg', null, null); 
	        Dom.Xmlnode reqNode = rootNode.addChildElement('csreai:header', null, null);
	    	try{
		    	String xml = req.params.get('csrXml');
		    	
		    	/*
		    	   load the xml:-
		    	*/
		    	xmldom d = new xmldom(xml); 
		    	
		    	String eventCode= d.getElementsByTagName('eai_event_code')[0].nodeValue;
		    	Case c; 
		    	/*
		    		if event code is XUPDSR - then update the case
		    	*/
		    	if(eventCode == APIConstants.EVTCODE_4_NEW){ 
		    		/*
		    		  create a service request here :-
		    		*/
		    		c= createServiceRequest(d);
		    		c=ServiceRequestsHelper.getRequest(c.Id);
		    		
		    	}else if(eventCode ==APIConstants.EVTCODE_4_UPDATE){
		    		String caseRequestId = d.getElementsByTagName('reference_id')[0].nodeValue;
		    		c=ServiceRequestsHelper.getRequest(caseRequestId);
		    		c.status = d.getElementsByTagName('Status_code')[0].nodeValue;
		    		update c;
		    	}
	    	
	    	
		        reqNode.addChildElement('csreai:reference_id', null, null).addTextNode(c.CaseNumber);
		        reqNode.addChildElement('csreai:event_date', null, null).addTextNode('');
		        reqNode.addChildElement('csreai:sr_type_code', null, null).addTextNode(c.SRType__r.Name);
		        reqNode.addChildElement('csreai:event_type_code', null, null).addTextNode('RESPONSE');
		        reqNode.addChildElement('csreai:eai_event_code', null, null).addTextNode('UPDSR');
		        reqNode.addChildElement('csreai:source_code', null, null).addTextNode('PRIMAPP');
		        reqNode.addChildElement('csreai:target_code', null, null).addTextNode('TRACKSTER');
		        reqNode.addChildElement('csreai:status_code', null, null).addTextNode(c.Status);
		        reqNode.addChildElement('csreai:details', null, null).addTextNode(c.description==null?'':c.description);
		        
		        //body
		        Dom.Xmlnode body = rootNode.addChildElement('csreai:body', null, null);
		       
		        Dom.Xmlnode inst = body.addChildElement('Document', null, null).addChildElement('Prc_instance', null, null);
		        inst.addChildElement('Prc_number', null, null).addTextNode(c.CaseNumber);
		        inst.addChildElement('Status_code', null, null).addTextNode(c.Status);
	    	}catch(Exception e){
	    		system.debug(e.getStackTraceString());
	    		reqNode.addChildElement('csreai:status_code', null, null).addTextNode('ERROR');
		        reqNode.addChildElement('csreai:details', null, null).addTextNode(e.getMessage());
	    	}
	        return doc.toXMLString();	    	 
	    }
	    
	    
	    public static Case createServiceRequest(xmldom d){
	    		/*
	    		 helper method that creates a service request:-	    		 
	    		*/
	    		String serviceCode = d.getElementsByTagName('sr_type_code')[0].nodeValue;
	    		Case c = new Case();
	    		
	    		ServiceRequestType__c stype= ServicesHelper.getService(serviceCode);
	    		
	    		XMLdom.Element servicerequest = d.getElementsByTagName('Prc_instance') [0];
	    		
	    		
	    		c.srtype__c = stype.id;
	    		c.status = servicerequest.getElementsByTagName('Status_description')[0].nodeValue;
	    		c.description = servicerequest.getElementsByTagName('Details')[0].nodeValue;
	    		c.subject = servicerequest.getElementsByTagName('Prc_description')[0].nodeValue;
	    		c.Priority = servicerequest.getElementsByTagName('Priority_description')[0].nodeValue;
	    		c.Origin = servicerequest.getElementsByTagName('Method_received_code')[0].nodeValue;
	    		
	    		String createdBy = servicerequest.getElementsByTagName('Created_by_description')[0].nodeValue;
	    		List<String> email_name = createdBy.split('-');
	    		
	    		Map<String,STring> contact_m = new Map<String,STring>();
	    		contact_m.put('email',email_name[0].trim());
	    		contact_m.put('last_name',email_name[1].trim());
	    		Contact con = ServiceRequestsHelper.createServiceRequestContact(contact_m);
	    		c.ContactId= con.id;
	    		
	    		//GEOLOCATION
	    		XMLdom.Element geo =  servicerequest.getElementsByTagName('Geo_location')[0];
	    		c.address__c = geo.getElementsByTagName('Display_address')[0].nodeValue;
	    		c.addressId__c = geo.getElementsByTagName('Location_id')[0].nodeValue;
	    		//c.geolocation__latitude__s = decimal.valueof(geo.getElementsByTagName('X_value')[0].nodeValue);
	    		//c.geolocation__longitude__s = decimal.valueof(geo.getElementsByTagName('Y_value')[0].nodeValue);
	    		
	    		insert c;
	    		
	    		//attributes to add where?
	    		
	    		//create activities:-
	    		for(XMLdom.Element el : servicerequest.getElementsByTagName('Prc_activity')){
	    			
	    		}
	    		
	    		//participants where ??
	    		
	    		return c;
	    		
	    }
		

}