public class MapsController {
    public List<String> listOfAddresses {get; set;}
    public List<String> listOfAccNames {get;set;}
	public Map<String,String> addressMap{get;set;}
    public MapsController() {
       listOfAddresses = new List<String>();
       listOfAccNames = new List<String>();
       addressMap = new Map<string,String>();
       //for(Case obj:[Select Id,Service_Code__c,Address__c from Case where (Address__c !=null OR Address__c!='') limit 5]){
       for(Case obj:[Select Id,Service_Code__c,Address__c from Case limit 5]){
       		listOfAccNames.add(obj.Service_Code__c);
            listOfAddresses.add('\"'+obj.Address__c+'\"');
            addressMap.put('\"'+obj.Address__c+'\"','\"'+obj.Address__c+'\"');
        }
       	system.debug('------------------'+listOfAddresses);
       
       //listOfAddresses.add('\"1900 M STREET SE,WASHINGTON,DC 20003\"');
       //listOfAddresses.add('\"700 JACKSON PLACE NW,WASHINGTON,DC,USA,20006\"');
      // listOfAddresses.add('\"1964 INDEPENDENCE AVENUE SW,WASHINGTON,DC 20004\"');
    }
  }