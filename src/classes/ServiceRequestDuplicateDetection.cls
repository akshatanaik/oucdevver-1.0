public with sharing class ServiceRequestDuplicateDetection {
	/*  
    *     
    *   Replace Methods Description here
    *   CreatedBy:D M
    *   Copyright ?   All rights reserved.
	*/
	
	public list<case>getDuplicateServiceRequests(string srAddress,string ServiceTypeName,decimal sRLongitude,decimal sRLatitude){
		list<case>serviceRequestList=new list<case>();
		try{
			
			ServiceRequestType__c serviceType=getService(ServiceTypeName);
			
			string queryStr=buildGetQueryRoot();
			
			//filter logic 
			queryStr+=' Where IsClosed=false';
			queryStr+=' And SRType__r.Service_Name__c =\'' +serviceType.Service_Name__c+'\' ';
			
			if(sRLatitude!=null && sRLongitude!=null){
				queryStr+='And ( DISTANCE(GeoLocation__c, GEOLOCATION(' + String.valueOf(sRLatitude) + ',' + String.valueOf(sRLongitude) + '),' + '\'mi' + '\') < 1) order by address__c';
			}
			
			// some more logic needs to added
			if(serviceType.Duplicate_Threshold_Period__c!=null){
				//SR?s, Case.createddate >= Todays Date - Threshold Days.
				String dateframe = DateTime.newInstance(Date.today().addDays(-integer.valueof(serviceType.Duplicate_Threshold_Period__c)), Time.newInstance(0, 0, 0, 0)).format('yyyy-MM-dd');
                queryStr+='And createddate >='+dateframe ;
			}
			
			if(serviceType.DuplicateRadius__c!=null){
				//queryStr+='And SRType__r.DuplicateRadius__c=\''+string.valueof(1/5280)+'\' ';
			}
			
			system.debug('queryresults-->'+queryStr);
			serviceRequestList=database.query(queryStr);
		}
		catch (exception e){
			system.debug('unable to complete query process due-->'+e);
		}
		return serviceRequestList;
	}
	
	// building query string 
	private string buildGetQueryRoot(){
		string query='Select c.Status, c.CaseNumber, c.address__c,c.SRType__r.Duplicate_Threshold_Period__c, c.SRType__r.DuplicateRadius__c, c.SRType__r.Service_Name__c, c.SRType__c, c.IsClosed,'; 
		query+='c.GeoLocation__c, c.GeoLocation__Longitude__s, c.GeoLocation__Latitude__s, c.CreatedDate,c.SRType__r.DuplicateDetectionMethod__c From Case c';
		return query;
	}
	
	// retrieve service request type based on service name
	public static ServiceRequestType__c getService(String serviceName){
        List<ServiceRequestType__c> srtypes= [Select Id,Name,Service_Name__c,Flex_Note_Count__c,External_Description__c,Flex_Notes_Link_Indicator__c,group__c,keywords__c,description__c,Agency__c,SLA__c,DuplicateDetectionMethod__c,Duplicate_Threshold_Period__c,DuplicateRadius__c FROM ServiceRequestType__c where Service_Name__c=:serviceName LIMIT 1];
        if(srtypes.isEmpty()){
            //invalid service code provided data could not be found
            throw new CustomException(APIConstants.NOT_FOUND);          
        }
        return srtypes[0]; 
    } 
}