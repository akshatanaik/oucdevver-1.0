/**
 * Class containing tests for ServiceRequestController
 */
@IsTest public with sharing class TestServiceRequestController {
   
    

   // getting all service types if option1 size is not zero
   @IsTest(SeeAllData=true) static void testServiceRequestItems_for_option_zero() {
        ServiceRequestController controller = new ServiceRequestController();  
        
        ServiceRequestType__c  TestServiceRequestType  = new ServiceRequestType__c (Object__c='Case',Name = 'A0143',Service_Name__c='Big Bulky',description__c='Please use this service request to Test service request controller.');
        INSERT TestServiceRequestType ;
        
        
        controller.getServiceRequestItems();
        /*
        Case c= new case(); 
		c= ServiceRequestsHelper.getRequest('');
		c=ServiceRequestsHelper.getRequestByToken('');
		list<Case> lc = ServiceRequestsHelper.listAllRequests('');
		*/
   }
   
   // getting all service types if option1 size is zero
   @IsTest(SeeAllData=true) static void testServiceRequestItems_for_option_not_zero() {  
       
        ServiceRequestController controller = new ServiceRequestController();  
        
        controller.getServiceRequestItems();
        Case c= new case(); 
        /*
        c= ServiceRequestsHelper.getRequest('12323');
		c=ServiceRequestsHelper.getRequestByToken('1231231');
		list<Case> lc = ServiceRequestsHelper.listAllRequests('123123123');
		*/
   }
   
    //get service type
   @IsTest(SeeAllData=true) static void testgetServiceType() {
        ServiceRequestController controller = new ServiceRequestController();  
        
        //selected service type
        controller.serviceType = 'Bulk Collection';
        
        controller.getServiceType();
   }
   
    //set service type
   @IsTest(SeeAllData=true) static void testsetServiceType() {
        ServiceRequestController controller = new ServiceRequestController();  
        
        //selected service type
        controller.serviceType = 'Bulk Collection';
        
        controller.setServiceType(controller.serviceType);
   }
   
    //get service type
   @IsTest(SeeAllData=true) static void testperformAction() {
        ServiceRequestController controller = new ServiceRequestController();  
     try{
             controller.performAction();
        //selected service type
        controller.serviceType = 'Bulk Collection';

        controller.setServiceType('Bulk Collection');
        controller.performAction();
       
        
        //selected service type
        controller.serviceType = '';
        
        controller.performAction();
        }catch(Exception e){}
   }
    //get service type
   @IsTest(SeeAllData=true) static void testretrieveFlexNoteQuestions() {
        ServiceRequestController controller = new ServiceRequestController();  
        
        controller.selectedServiceRequestType= '';
       
        controller.retrieveFlexNoteQuestions();
        
       /* ServiceRequestTypeFlexNote__c  TestServiceRequestTypeFlexNote  = new ServiceRequestTypeFlexNote__c (Service_Request_type__c='',IsRequiredQuestion__c= '',FlexNote_Question__c='',isRequired__c='');
        INSERT TestServiceRequestTypeFlexNote ;*/
      
   }
}