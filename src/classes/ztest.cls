@RestResource(urlMapping='/v2/ztest/*')
global with sharing class ztest{
    
    @HttpGet
    
    global static service getServices() {
         RestRequest req = RestContext.request; 
         RestResponse response = RestContext.response;   
         
         String format = req.requestURI.split('\\.').size()>1?req.requestURI.split('\\.')[1]:'';
         
         String responseStr;
         
         /*
          check to see whether jurisdiction_id is passed
         */
         String jId = req.params.get('jurisdiction_id');
         
         /*
           validate that the Juridictoion Id is correct 
         */
         if(jId!=null && !APIValidations.isValidJuridiction(jId)){
            //invalid juridiction provided:-
            response.statuscode =404; 
            //RestContext.response.responseBody =   Blob.valueOf(CustomException.sendJSONApiErrorResponse('Invalid jurisdiction_id provided'));  
            return null;
         }  
         
         try{ 
             // see if a service code was  part of the URI
             String serviceCode= req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
             serviceCode = serviceCode.split('\\.').size()>1?serviceCode.split('\\.')[0]:serviceCode;          
             /*
             if (serviceCode!= '' && !serviceCode.startsWith('services')) {
                  //we have the service code here  : this is always done only if metadat =true
                  if(req.headers.get('Content-Type')==APIConstants.CONTENT_TYPE_JSON || format=='json')  
                    responseStr= Open311_ServicesAPIResponse.sendJSONResponse(ServicesHelper.getService(serviceCode),ServicesHelper.getServiceMetadata(serviceCode)); 
                  else
                    responseStr= Open311_ServicesAPIResponse.sendXMLResponse(ServicesHelper.getService(serviceCode),ServicesHelper.getServiceMetadata(serviceCode)); 
             } else {                
                  if(req.headers.get('Content-Type')==APIConstants.CONTENT_TYPE_JSON || format=='json') 
                    responseStr= Open311_ServicesAPIResponse.sendJSONResponse(ServicesHelper.listAllServices(jId)); 
                  else
                    responseStr= Open311_ServicesAPIResponse.sendXMLResponse(ServicesHelper.listAllServices(jId));                 
             } */
             
             return sendResponse(ServicesHelper.listAllServices(jId)); 
             
              
         }catch(CustomException e){             
                if(APIConstants.NOT_FOUND==e.getMessage()){
                    response.statuscode =404;   
                    //responseStr= CustomException.sendJSONApiErrorResponse('Invalid service_code provided');            
                }               
         } 
        
         //RestContext.response.responseBody =   Blob.valueOf(responseStr);  
         return null;
    }
    
    global class zTestResponse{
    public String service_code;  //1
    public String service_name;  //Cans left out 24x7
    public String description;  //Garbage or recycling cans that have been left out for more than 24 hours after collection. Violators will be cited.
    public boolean metadata;
    public String type;  //realtime
    public String keywords;  //lorem, ipsum, dolor
    public String grp;  //sanitation
  } 
  
  global class service {
    List<zTestResponse> service = new List<zTestResponse>();
    
    public service(List<zTestResponse> responseList) {
      service = responseList;
    }
  }

  public static service sendResponse(List<ServiceRequestType__c> obj){
    List<zTestResponse> response = new List<zTestResponse>();
    for(ServiceRequestType__c req:obj){
      zTestResponse r = new zTestResponse();
      r.service_code = req.name;
      r.service_name = req.Service_Name__c;
      response.add(r);
    }
    return new service(response);
    
  }
    
}