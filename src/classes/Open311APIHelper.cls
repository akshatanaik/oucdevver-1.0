public with sharing class Open311APIHelper {

		/*
		  some utility methods to be used across open311 helper
		*/
		/**
		    @brief Convert an ISO 8601 timestamp to an Apex \c DateTime value.
		
		    Uses the JSONParser getDateTimeValue() method.
		
		    See http://en.wikipedia.org/wiki/ISO_8601 for timestamp format details.
		
		    Throws a System.JSONException if \p iso8601_ts is not a valid ISO 8601
		    timestamp.
		
		    @param iso8601_ts Valid ISO 8601 timestamp.
		
		    @return a \c DateTime value for the given timestamp
		*/
		public static DateTime convertTimestamp( String iso8601_ts)
		{
		    // Create a valid JSON string like '{"t":"2012-07-31T11:22:33.444Z"}'
		    JSONParser parser = JSON.createParser( '{"t":"' + iso8601_ts + '"}');
		
		    parser.nextToken();    // advance to the start object marker
		    parser.nextValue();    // advance to the next value
		
		    // Bug in JSONParser or DateTime object results in a malformed DateTime,
		    // so convert to Long and back to DateTime.  Without the conversion,
		    // methods that access timeGmt() and its components will actually get
		    // local time instead of GMT.
		    return DateTime.newInstance( parser.getDateTimeValue().getTime());
		
		    // Once bug is fixed, this return is preferred.
		    // return parser.getDateTimeValue();
		}
		
		/*
	        @Purpose:-
	        validate api key
	        
	        contains the logic for validation api key and updating response in case
	        of error
	        
	        returns true if everything is fine / false in error
	   */   
	    public static APIValidations.APIKeyResponse checkAPIKey(String apiKey,RestRequest req,RestResponse response,String format){
	    	if(apiKey==null || apiKey=='' ){
	         	//invalid apikey provided:-
	            response.statuscode =400; 
	            String responseStr =  req.headers.get('Content-Type')==APIConstants.CONTENT_TYPE_JSON || format=='json'?
	                    CustomException.sendJSONApiErrorResponse(' api_key not  provided'):
	                    CustomException.sendXMLApiErrorResponse('api_key not  provided'); 
	            response.responseBody =  Blob.valueOf(responseStr);   
	           
	            return null;     
	         }
	         
	         APIValidations.APIKeyResponse apiKeyRes = APIValidations.validateKey(apiKey);
	         if(apiKeyRes.isValid==false){
	         	//we have an invalid api key provided
	         	response.statuscode =400; 
	            String responseStr =  req.headers.get('Content-Type')==APIConstants.CONTENT_TYPE_JSON || format=='json'?
	                    CustomException.sendJSONApiErrorResponse(' Invalid api_key  provided'):
	                    CustomException.sendXMLApiErrorResponse(' Invalid api_key  provided'); 
	            response.responseBody =  Blob.valueOf(responseStr); 
	            
	            logRequest(req,'Error','Invalid api_key provided');
	            
	            return null;   
	         }
	         
	         return apiKeyRes;
	    }
 
 		/*
	        @Purpose:-
	        Create log records	       	            
	   */   
 		public static void logRequest(RestRequest req,String status,String message){
 			/*
 				create a log object
 			*/
 			Open311_API_Log__c log= new Open311_API_Log__c();
 			log.Log__c=message;
 			log.status__c=status;
 			log.apikey__c=req.params.get('api_key');
 			log.Endpoint__c=req.requestURI;
 			log.response_format__c=req.requestURI.split('\\.').size()>1?req.requestURI.split('\\.')[1]:'xml';
 			log.Request_Method__c=req.httpMethod;
 			log.Request_Param__c = JSON.serialize(req.params);
 			insert log;
 		}

}