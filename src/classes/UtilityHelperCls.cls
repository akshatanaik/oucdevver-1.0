public class UtilityHelperCls {

	/**
   * Queries for the specified Decode by Name and returns the corresponding
   * response object.
   *
   * @param name  DecodeObject's Name
   * @return    A response version of the matching DecodeObject.
   *            If no records are found, it will simply return a custom error message that the DecodeObject
                is invalid
   */
	public static Decode_Object__c getDecodeObject(string nameStr){
		list<Decode_Object__c>decodeObjList=[select id,Active__c,Code__c,Order__c,Short_Description__c,Type__c,Value__c from Decode_Object__c where name=:nameStr limit 1];
		if(decodeObjList.isEmpty()){
			// exception as to include here
		}
		return decodeObjList[0];
	}
	
	public list<selectOption>getPickListValues(string decodeName){
		list<selectOption>selectedOptions=new list<selectOption>();
		try{
			if(decodeName!=null){
				string decodeValue=getDecodeObject(decodeName).Value__c;
				selectedOptions.add(new selectOption (decodeValue,decodeValue)); 
			}
			if(selectedOptions==null)return null;
		}
		catch(exception e){
			system.debug('unable to complete retrieve process due--'+e);
		}
	    return selectedOptions;
	}
	
	public map<string,string>getCodeDescriptionMap(string decodeName){
		map<string,string>codeDecriptionMap=new map<string,string>();
		try{
			if(decodeName!=null){
				Decode_Object__c decodeObj=getDecodeObject(decodeName);
				if(decodeObj!=null){
					string decodeCode=decodeObj.Code__c;
					string decodeShortDescription=decodeObj.Short_Description__c;
					if(decodeCode!=null && decodeShortDescription!=null)codeDecriptionMap.put(decodeCode,decodeShortDescription);
				}
			}
		}
		catch(exception e){
			system.debug('unable to complete map retrieve process due--'+e);
		}
		if(codeDecriptionMap.isEmpty())return null;
		return codeDecriptionMap;
	}
}