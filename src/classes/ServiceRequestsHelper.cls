public class ServiceRequestsHelper{
 
    /**
       * Queries for the specified request by service request id(case id) and returns the corresponding
       * response object.
       *
       * @param id  request's id
       * @return    A response version of the matching request.
       *            If no records are found, it will simply error message saying invalid/not found
       */
       public static Case getRequest(String requestId){
            
            string query=buildGetQueryRoot();
            query+='WHERE CaseNumber=\'' +requestId +'\' ';

            List<Case> cases = database.query(query);
                               
            if(cases.size()==0){
                 //invalid requestId code provided data could not be found
                  throw new CustomException(APIConstants.NOT_FOUND); 
            }
            return cases[0];
            
       }
       
       public static Case getRequest(Id requestId){
            
            string query=buildGetQueryRoot();
            query+='WHERE Id=\'' +requestId +'\' ';

            List<Case> cases = database.query(query);
                               
            if(cases.size()==0){
                 //invalid requestId code provided data could not be found
                  throw new CustomException(APIConstants.NOT_FOUND); 
            }
            return cases[0];
            
        }
   
        private static String buildGetQueryRoot(){
            string query = 'Select casenumber,status,description,Subject,AgencyCode__c,';
            query+='SRType__r.Name,SRType__r.Service_Name__c,CreatedDate,LastModifiedDate,';
            query+='Last_Update_Date__c,ClosedDate,IsClosed,Address__c,AddressID__c,geolocation__latitude__s,reason,';
            query+='geolocation__longitude__s,media_url__c,Expected_Resolution_Date__c,Contact.firstname,Contact.lastname,';
            query+='Contact.Phone,Contact.Email,Priority,Origin,Device__c,SLA__c,External_Id__c,External_System_Name__c,Language__c,';
            query+='IntersectionId__c,Quadrant__c,Ward__c,PolDistrict__c,PSA__c,ANC__c,SMD__c,Cluster__c,XCOORD__c,YCOORD__c,Zipcode__c,';
            query+='SRType_Jurisdiction__r.Jurisdiction__c,SRType_Jurisdiction__r.Jurisdiction__r.Department__c,Jurisdiction__c,';
            query+='SRType_Jurisdiction__r.Jurisdiction__r.Name,';
            query+='(Select FlexNote_question__r.Name, questions__c,Order__c, flexNote_question__r.Questions__c, Answer__c, ';
            query+='flexNote_question__r.Sequence_Number__c,FlexNote_question__r.Answer_Type__c,CodeDescription__c FROM FlexNotes__r ORDER BY Order__c, ID  ),';
            query+='(Select Id,Status__c,Task_Code__c,Task_Short_Name__c,Citizen_Email_On_Complete__c,';
            query+='Description__c,external_comments__c,outcome__c,Display_Sort_order__c,completion_date__c,';
            query+='Due_Date__c,Responsible_Party__c FROM OUC_Activities__r ORDER BY Display_Sort_Order__c )';
            query+='FROM Case ';
            
            return query;
            
        }
        /**
       * Queries for the specified request by service request token and returns the corresponding
       * response object.
       *
       * @param id  request's token
       * @return    A response version of the matching request.
       *            If no records are found, it will simply error message saying invalid/not found
       */
        public static Case getRequestByToken(String requestToken){
            
            List<Case> cases =[Select casenumber,status,description,Subject,
                               SRType__r.Name,SRType__r.Service_Name__c,CreatedDate,
                               LastModifiedDate,ClosedDate,Address__c,addressid__c,geolocation__latitude__s,
                               geolocation__longitude__s,media_url__c
                               FROM Case WHERE CaseNumber=:requestToken];
                               
                               
            if(cases.size()==0){
                 //invalid request Id code provided data could not be found
                throw new CustomException(APIConstants.NOT_FOUND);     
            }
            return cases[0];
            
        }
        
        /**
           * Queries for the specified contact by whom service request is(case id) created and returns the corresponding
           * response object.
           *
           * @param id  map with key first_name,last_name,phone,email
           * @return    A response version of the matching request.
           *            If no records are found, it will create a new contact
           */
        
        public static Contact createServiceRequestContact(Map<String,String> contactParams){
                
                //check if the contact exist
                List<Contact> contact =[Select Id,Name FROM Contact WHERE email=:contactParams.get('email')];
                
                if(contact.size()>0){
                    // ER:  Need to consider if provided email exists in multiple contacts 
                    //      AND scenario where input information for first_name, Last_name, address, etc might be different from contact record
                    //  
                    contact[0].lastname = contactParams.get('last_name');
                    contact[0].firstname = contactParams.get('first_name');
                    contact[0].email = contactParams.get('email');
                    contact[0].phone = contactParams.get('phone');
                    contact[0].MailingStreet = contactParams.get('mailing_Street');
                    contact[0].MailingCity = contactParams.get('mailing_City');
                    contact[0].MailingState = contactParams.get('mailing_State');
                    contact[0].MailingCountry = contactParams.get('mailing_Country'); 
                    contact[0].OtherPhone = contactParams.get('other_Phone');
                    
                    upsert contact[0];  
                     
                    return contact[0];
                }
                
                //other wise create a contact:-
                Contact con = new Contact();
                con.lastname = contactParams.get('last_name');
                con.firstname = contactParams.get('first_name');
                con.email = contactParams.get('email');
                con.phone = contactParams.get('phone');
                
                // Included address
                con.MailingStreet = contactParams.get('mailing_Street');
                con.MailingCity = contactParams.get('mailing_City');
                con.MailingState = contactParams.get('mailing_State');
                con.MailingCountry = contactParams.get('mailing_Country');
                con.OtherPhone = contactParams.get('other_Phone');
                
                //create account for a contact
                Account acc = createPersonalAccount(contactParams);
                system.debug('accountList ----->'+acc );
                con.AccountId = acc.Id;
                
                insert con;
                
                return con; 
                
        }
        
        /**
       * Queries for list of Requests records and and returns the corresponding
       * response object (list of Requests).  If any non-null parameters are
       * provided, the list will be filtered by the specified field.
       *
       * @param jurisdiction_id    Id of jurisdiction
       * @return              Collection of Requests objects, as an list
       */
        public static List<Case> listAllRequests(String filter){
            /*String query =    'Select  casenumber,status,description,Subject,AgencyCode__c,';
            query+=' SRType__r.Name,SRType__r.Service_Name__c,CreatedDate,Priority,Origin,Contact.firstname,';
            query+=' Contact.lastname,Contact.Phone,Contact.Email,Last_Update_Date__c,ClosedDate,IsClosed,Address__c,AddressID__c,';                     
            query+=' geolocation__latitude__s,geolocation__longitude__s,media_url__c,Expected_Resolution_Date__c,Device__c,SLA__c,External_Id__c,';                  
            query+=' External_System_Name__c,Language__c,IntersectionId__c,';
            query+=' Quadrant__c,Ward__c,PolDistrict__c,PSA__c,ANC__c,SMD__c,Cluster__c,XCOORD__c,YCOORD__c,' ;
            query+=' (Select FlexNote_question__r.Name, questions__c, flexNote_question__r.Questions__c, Answer__c, ';
            query+=' flexNote_question__r.Sequence_Number__c,FlexNote_question__r.Answer_Type__c FROM FlexNotes__r ORDER BY Order__c, ID ),';
            query+=' (Select Id,Status__c,Task_Code__c,Task_Short_Name__c,Citizen_Email_On_Complete__c,';
            query+='Description__c,external_comments__c,outcome__c,Display_Sort_order__c,completion_date__c,';
            query+='Due_Date__c,Responsible_Party__c FROM OUC_Activities__r ORDER BY Display_Sort_Order__c )';
            
            query+=' FROM Case ';
            */
            string query=buildGetQueryRoot();
            
            //adding filter
            if(filter==null){
                query +=' WHERE CreatedDate >= LAST_90_DAYS ';
            }
            
            if(filter!=null){
                query +=' WHERE '+filter;
            }
            
            // ER:  order by needs to by Last_Update_Date__c ASC when Update_Date is passed to GET SERVCIES
            if(filter.containsIgnoreCase('Last_Update_Date__c'))
                query+=' ORDER BY Last_Update_Date__c ASC ' ; // LIMIT 1000  (need to evaluate if we need LIMIT on this - LIKELY we do)
            else
                query+=' ORDER BY CreatedDate DESC LIMIT 1000 ';
            system.debug('FILTER:' + filter);
            
            query+=' LIMIT 400';
            
            return database.query(query);
            
        } 
       
        public static account createPersonalAccount(map<string,string>contParms){
            list<account>account=[select id,name from account where name=:contParms.get('firstname')+' '+contParms.get('lastName')];
            if(account.size()>0)return account[0];
            Account acc = new Account();
               acc.Name=contParms.get('firstname')+' '+contParms.get('lastname');
               insert acc;
               return acc;   
        }
        
        /**
       * Queries for the specified BulkSchedule__c by whom service request's(Ward__C) and returns the corresponding
       * response object.
       * 
       *
       * @param     wrd  (Ward of ServiceRequest), configName (Custom Setting Record Name for daily Stop limit i.e DailyLimit)
       * @return    A response version of the matching request. 
       *            If no records are found, it will return empty sObject
       */
        
        
        public static sObject getFlexNoteDefault(String wrd,string configName){
            sObject obj;
            for(BulkSchedule__c bs :[select id,Name,ServiceRequestBulkScheduleCount__c,ScheduleDate__c,Ward__c from BulkSchedule__c where ScheduleDate__c > :system.today() Order By ScheduleDate__c ASC ]){
                if(bs.Ward__c.contains(wrd)){
                    if(bs.ServiceRequestBulkScheduleCount__c<OUCServiceRequestConfig__c.getValues(configName).BulkScheduleDailyStopLimit__c && bs.ScheduleDate__c > system.today().addDays(Integer.valueof(OUCServiceRequestConfig__c.getValues(configName).NumberofDays__c))){
                        system.debug('result---'+bs);
                        obj=(sObject)bs;
                        return obj;
                    }
                }
            }    
                    
            if(obj==null){ 
            // i needed check this later
              // throw new CustomException(APIConstants.NOT_FOUND);     
            }
            // if no record found ;
            //return (sObject) new BulkSchedule__c();  
            return null;                        
        }
        
          
        
        public static sObject getFlexNoteDefault( String Flexnote_Code){
            //if(Flexnote_Code==Bulk Collection FlexNote){}
            list<BulkSchedule__c>bulkSchedule=[select id,ScheduleDate__c,Ward__c from BulkSchedule__c where ScheduleDate__c > :system.today() Order By ScheduleDate__c ASC limit 1];
            if(bulkSchedule.size()>0){
                sObject obj=(sObject)bulkSchedule[0];
                return obj;
            }
            
             //invalid request Id code provided data could not be found
            throw new CustomException(APIConstants.NOT_FOUND);     
        } 
        
        
        
        public static map<Id,String> getQuestionCodeAnswerMap(LIST<FlexNoteQuestion__c> fq ){
             map<Id,String>questionCodeAnswerValueMap=new map<Id,String>();
             String answerValues; 
                         
             if(!fq.isEmpty()){ 
                
                 List<String> av = new   List<String> ();
                 for(FlexNoteQuestion__c f:fq)av.add(f.AnswerValues__c);
                 
                 list<Decode_Object__c> doc =[Select Id,Name,Code__c,Value__c from Decode_Object__c  d  where Name IN :av order By Name]; 
                 map<String, List<Decode_Object__c>> decodeObjMap = new map<String, List<Decode_Object__c>>();
                
                 for(Decode_Object__c f:doc){ 
                    //decodeObjMap.put(f.Name,decodeObjMap.get(f.Name)==null? new List<Decode_Object__c>{f}:decodeObjMap.get(f.Name).add(f));
                    if(decodeObjMap.containsKey(f.Name)){
                        decodeObjMap.get(f.Name).add(f);
                    }/*else{
                        decodeObjMap.put(f.Name,new list<Decode_Object__c>{f});
                    }*/
                 }     
                 
                 for(FlexNoteQuestion__c f:fq){
                    // case 1
                    // if flexnote question contains values 
                    if(f.AnswerValues__c != null && f.AnswerValues__c.contains(',')){
                         answerValues = f.AnswerValues__c;
                         answerValues  = answerValues.replace( ',' , ';' );                
                         system.debug('Log0 : answerValues1  ------------->'+answerValues  ); 
                         questionCodeAnswerValueMap.put(f.Id,answerValues);  
                     } 
                     // case 2
                     // if flexnote question options values are coming from decode object
                     if(f.AnswerValues__c != null && !(f.AnswerValues__c.contains(','))){
                          //answerValues = f.AnswerValues__c;                     
                          if(decodeObjMap!=null){
                           if(decodeObjMap.get(f.AnswerValues__c)!=null){
                              for(Decode_Object__c d :decodeObjMap.get(f.AnswerValues__c)){
                                  answerValues  +=d.Code__c+':'+d.Value__c+';';
                                  system.debug('Log0 : answerValues2  ------------->'+answerValues  );  
                              }
                            }
                          }
                          questionCodeAnswerValueMap.put(f.Id,answerValues);
                          
                     }           
                 }
             }
               
             return questionCodeAnswerValueMap;      
        }
        
        public static void startFlexNoteProcess(list<FlexNote__c>listFlexNotes,list <FlexNoteQuestion__c> fnList){
            //map<Id,FlexNoteQuestion__c>flexNoteQuestionMap=new map<Id,FlexNoteQuestion__c>([Select Questions__c, Object_Name__c, Name, Answer_Type__c, AnswerValues__c From FlexNoteQuestion__c  where  Id IN (Select FlexNote_Question__c From FlexNote__c where Id IN : listFlexNotes) ]);            
            map<Id,FlexNoteQuestion__c>flexNoteQuestionMap=new map<Id,FlexNoteQuestion__c>(fnList);
            map<string,list<Decode_Object__c>>decodeObjectMap=new map<string,list<Decode_Object__c>>();
            list<STRING>listAnswerValue=new list<STRING>();
            FOR(FlexNoteQuestion__c FN: flexNoteQuestionMap.Values()){
                listAnswerValue.add(FN.AnswerValues__c);
            }
        
            list<Decode_Object__c>listDecodeObject=new list<Decode_Object__c>([Select d.Value__c, d.Type__c, d.Short_Description__c, d.Order__c, d.Name, d.Code__c, d.Active__c From Decode_Object__c d where name IN :listAnswerValue]);
        
            for(Decode_Object__c f:listDecodeObject){
                //if(decodeObjectMap.containsKey(f.Name) ? decodeObjectMap.get(f.Name).add(f):decodeObjectMap.put(f.Name,new list<Decode_Object__c>{f});
                if(decodeObjectMap.containsKey(f.Name)){
                    decodeObjectMap.get(f.Name).add(f);
                }else{
                    decodeObjectMap.put(f.Name,new list<Decode_Object__c>{f});
                }
            
            }
            system.debug('listDecodeObject---'+decodeObjectMap);
            //invoke codedescription method
            setFlexNoteCodeDescriptionValues(listFlexNotes,flexNoteQuestionMap,decodeObjectMap);
            
        }
    
        private static void setFlexNoteCodeDescriptionValues(list<FlexNote__c>listFlexNotes,map<Id,FlexNoteQuestion__c>flexNoteQuestionMap,map<string,list<Decode_Object__c>>decodeObjectMap){
            // updating code description based on flexnotequestion
            list<Decode_Object__c>decodedList=getDecodeList(decodeObjectMap);
            for(FlexNote__c oFlexNote:listFlexNotes){
                FlexNoteQuestion__c fnQ=flexNoteQuestionMap.get(oFlexNote.FlexNote_Question__c);
                
                if(oFlexNote.Answer__c!=null){
                    list<string>listAnswers=(oFlexNote.Answer__c!=null && oFlexNote.Answer__c.contains(',')) ? oFlexNote.Answer__c.split(',') : new list<string>{oFlexNote.Answer__c};
                    map<string,string>codeDescriptionMap=new map<string,string>();
                    string codeDescriptionValue='';
                   
                    // holds code and description values to be converted to json format
                    list<CodeDescriptionWrapper>codeDescriptionList=new List<CodeDescriptionWrapper>();
                    
                    // holds code and description 
                    map<string,string>codeDescriptionValueMap=new map<string,string>();
                    
                    
                    if(fnQ!=null){
                        
                        if(fnQ.AnswerValues__c!=null){
                            if(fnQ.Answer_Type__c!=null){
                                // datatype multipicklist processing 
                                if(fnQ.Answer_Type__c=='MultiPicklist'){
                                    if(fnQ.AnswerValues__c.contains(':')){
                                        //map<string,string>codeDescriptionMap=new map<string,string>();
                                        list<string>answersOptionsList=(fnQ.AnswerValues__c!=null && fnQ.AnswerValues__c.contains(',') ) ? fnQ.AnswerValues__c.split(','): new list<string>{fnQ.AnswerValues__c};
                                        // creating map of code description 
                                        for(string answerItems: answersOptionsList){
                                            if(answerItems.contains(':')){
                                                codeDescriptionMap.put(answerItems.split(':')[0],answerItems.split(':')[1]);
                                            }
                                        }
                                        // iterating through questions answervalues and matching with answer from flexnotes
                                        for(string cD:codeDescriptionMap.KeySet()){
                                            for(string ansItem:listAnswers){
                                                if(ansItem==cD){
                                                    codeDescriptionValue+=ansItem+':'+codeDescriptionMap.get(ansItem)+';';
                                                                                        
                                                    codeDescriptionList.add(new CodeDescriptionWrapper(ansItem,codeDescriptionMap.get(ansItem)));
                                                    codeDescriptionValueMap.put(ansItem,codeDescriptionMap.get(ansItem));
                                                }
                                            }
                                        }
                                        
                                    }
                                    else{
                                        // iterating through decodeObjectMap ,checking whether it contains given question answervalues
                                        if(decodeObjectMap.get(fnQ.AnswerValues__c)!=null){
                                            if(decodeObjectMap.containsKey(fnQ.AnswerValues__c)){
                                                //list<string>listAnswers=if(oFlexNote.Answer__c!=null && oFlexNote.Answer__c.contains(',')) ? oFlexNote.Answer__c.split(',') : new list<string>{oFlexNote.Answer__c};
                                                
                                                for(Decode_Object__c decodeObjectItem: decodedList){
                                                    for(string lA: listAnswers){
                                                        if(lA==decodeObjectItem.Code__c){
                                                            codeDescriptionValue+=decodeObjectItem.Code__c+':'+decodeObjectItem.Value__c+';';
                                                            
                                                            codeDescriptionList.add(new CodeDescriptionWrapper(decodeObjectItem.Code__c,decodeObjectItem.Value__c));
                                                            codeDescriptionValueMap.put(decodeObjectItem.Code__c,decodeObjectItem.Value__c);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else{
                                            ///if(oFlexNote.Answer__c!=null)codeDescriptionValue+=oFlexNote.Answer__c+';';
                                            //
                                            for(string lA: listAnswers){
                                                codeDescriptionValue+=lA+';';
                                                
                                                codeDescriptionList.add(new CodeDescriptionWrapper('',lA));
                                                codeDescriptionValueMap.put('',lA);
                                            }
                                        }
                                    }
                                }
                                
                                 // datatype picklist processing 
                                if(fnQ.Answer_Type__c=='Picklist'){
                                    if(fnQ.AnswerValues__c.contains(':')){
                                        //map<string,string>codeDescriptionMap=new map<string,string>();
                                        list<string>answersOptionsList=(fnQ.AnswerValues__c!=null && fnQ.AnswerValues__c.contains(',') ) ? fnQ.AnswerValues__c.split(','): new list<string>{fnQ.AnswerValues__c};
                                        for(string answerItems: answersOptionsList){
                                            if(answerItems.contains(':')){
                                                codeDescriptionMap.put(answerItems.split(':')[0],answerItems.split(':')[1]);
                                            }
                                        }
                                        for(string cD:codeDescriptionMap.KeySet()){
                                            for(string ansItem:listAnswers){
                                                if(ansItem==cD){
                                                    codeDescriptionValue+=ansItem+':'+codeDescriptionMap.get(ansItem)+';';
                                                    
                                                    codeDescriptionList.add(new CodeDescriptionWrapper(ansItem,codeDescriptionMap.get(ansItem)));
                                                    codeDescriptionValueMap.put(ansItem,codeDescriptionMap.get(ansItem));
                                                }
                                            }
                                        }
                                        
                                    }else{
                                        // iterating through decodeObjectMap ,checking whether it contains given question answervalues
                                        if(decodeObjectMap.get(fnQ.AnswerValues__c)!=null){
                                            if(decodeObjectMap.containsKey(fnQ.AnswerValues__c)){
                                                //list<string>listAnswers=if(oFlexNote.Answer__c!=null && oFlexNote.Answer__c.contains(',')) ? oFlexNote.Answer__c.split(',') : new list<string>{oFlexNote.Answer__c};
                                                for(Decode_Object__c decodeObjectItem: decodedList){
                                                    for(string lA: listAnswers){
                                                        if(lA==decodeObjectItem.Code__c){
                                                            codeDescriptionValue+=decodeObjectItem.Code__c+':'+decodeObjectItem.Value__c+';';
                                                            
                                                            codeDescriptionList.add(new CodeDescriptionWrapper(decodeObjectItem.Code__c,decodeObjectItem.Value__c));
                                                            codeDescriptionValueMap.put(decodeObjectItem.Code__c,decodeObjectItem.Value__c);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else{
                                            ///if(oFlexNote.Answer__c!=null)codeDescriptionValue+=oFlexNote.Answer__c+';';
                                            for(string lA: listAnswers){
                                                codeDescriptionValue+=lA+';';
                                                codeDescriptionList.add(new CodeDescriptionWrapper('',lA));
                                                codeDescriptionValueMap.put(lA,lA);
                                            }
                                        }
                                    }
                                }
                                
                                
                                
                                
                            }
                        }
                        
                        
                        // processing other than multipicklist and picklist datatype 
                        if(fnQ.Answer_Type__c!=null && fnQ.Answer_Type__c!='MultiPicklist' && fnQ.Answer_Type__c!='Picklist'){
                                
                                if(fnQ.AnswerValues__c!='' && fnQ.Answer_Type__c=='Text' ){
                                    if(fnQ.AnswerValues__c=='{!Value}'){
                                        for(string lA: listAnswers){
                                            codeDescriptionValueMap.put(String.valueOf(date.parse(lA)),String.valueOf(date.parse(lA)));
                                        }
                                    }
                                }
                                
                                if(fnQ.Answer_Type__c=='Date/Time'){
                                    for(string lA: listAnswers){
                                        codeDescriptionValueMap.put(String.valueOfGmt(DateTime.parse(lA)),String.valueOfGmt(DateTime.parse(lA)));
                                    }
                                }
                                
                                if(fnQ.Answer_Type__c=='Date'){
                                    for(string lA: listAnswers){
                                        codeDescriptionValueMap.put(String.valueOf(date.parse(lA)),String.valueOf(date.parse(lA)));
                                    }
                                }
                                
                                if(( fnQ.Answer_Type__c=='Text' &&  fnQ.AnswerValues__c!='{!Value}')){
                                    for(string lA: listAnswers){
                                        codeDescriptionValueMap.put(lA,lA);
                                    }
                                }
                                
                                if(( fnQ.Answer_Type__c=='TextArea' )   ){
                                    for(string lA: listAnswers){
                                        codeDescriptionValueMap.put(lA,lA);
                                    }
                                }
                                
                        }
                        
                        
                    }
                    
                    
                    String JSONString = JSON.serialize(codeDescriptionValueMap);
                    //String JSONString = JSON.serialize(codeDescriptionList);
                    System.debug('Serialized list of codeDescription into JSON format:-- ' + JSONString);
                    oFlexNote.CodeDescription__c=JSONString;
                    system.debug('codeDescription'+codeDescriptionValue);
                    
                    
                    //if(codeDescriptionValue.endsWith(';'))codeDescriptionValue=codeDescriptionValue.substring(0,codeDescriptionValue.length()-1);
                    //oFlexNote.CodeDescription__c=codeDescriptionValue;
                }
            }
        
        }                             
    
        private static list<Decode_Object__c>getDecodeList(map<string,list<Decode_Object__c>>decodeObjectMap){
            list<Decode_Object__c>tempDecodeObjectList=new list<Decode_Object__c>();
            for(string decodeName:decodeObjectMap.keySet()){
                tempDecodeObjectList.addAll(decodeObjectMap.get(decodeName));
            }
            return tempDecodeObjectList;
        }	
    	
    	
    	
    	// getting next business day based on sla and created date
    	public static void setExceptedBussinessDateTime(list<case> tempCaseList,list<ServiceRequestType__c>serviceTypeList){
    		// Assumes the Bussines work hours are the default for the Org
			// check whether its customer communtiy login license if not proceed,update expected bussines resolution dates
				
				list<BusinessHours> stdBusinessHourslist = [Select b.Name, b.Id From BusinessHours b where Name=:OUCServiceRequestConfig__c.getValues('DailyLimit').DefaultBusinesshoursName__c and isActive = true];
				
				if(stdBusinessHourslist.isEmpty())return;
				map<id,ServiceRequestType__c>serviceTypeMap=new map<id,ServiceRequestType__c>(serviceTypeList);
				for(case cs:tempCaseList){
					if ((cs.CreatedDate != NULL) && (stdBusinessHourslist[0] != NULL)) {
						string sla;
						
						if(cs.SLA__c==null){
							if(serviceTypeMap.get(cs.SRType__c)!=null){
								if(serviceTypeMap.get(cs.SRType__c).SLA__c!=null){
									sla=serviceTypeMap.get(cs.SRType__c).SLA__c;
								}
							}
						}
						else{
							sla=cs.SLA__c;
						}
						
						if(sla!=null){
							DateTime SLADateTime = BusinessHours.nextStartDate(stdBusinessHourslist[0].Id, cs.CreatedDate);
							long busDaysAs8HourMilliseconds = ( long.ValueOf(sla) * 8 * 3600 * 1000);
							SLADateTime = BusinessHours.addGMT (stdBusinessHourslist[0].id, SLADateTime, busDaysAs8HourMilliseconds);
							cs.ExpectedBusinessResolutionDate__c = SLADateTime;
							system.debug('c.ExpectedBusinessResolutionDate__c-->'+cs.ExpectedBusinessResolutionDate__c);
						}
					}
				}
    	}
    	
    	// fetching potential duplicate service request based on mar details and service type 
		public static list<case>getDuplicateServiceRequests(string srAddress,string ServiceTypeName,decimal sRLongitude,decimal sRLatitude){
			list<case>serviceRequestList=new list<case>();
			try{
				
				ServiceRequestType__c serviceType=getServiceType(ServiceTypeName);
				
				if(serviceType.DuplicateDetectionMethod__c!='Standard' || serviceType.DuplicateRadius__c==null){
					return new list<case>();
				}
				
				string queryStr=buildQueryRoot();
				
				//filter logic 
				queryStr+=' Where IsClosed=false';
				queryStr+=' And SRType__r.Service_Name__c =\'' +serviceType.Service_Name__c+'\' ';
				
				if(sRLatitude!=null && sRLongitude!=null && serviceType.DuplicateRadius__c!=null){
					
					// 1 mile  = 0.000189394 *foot
					decimal limitRadius =(serviceType.DuplicateRadius__c * 0.000189394);
					system.debug('radius--->'+limitRadius);
					
					queryStr+='And ( DISTANCE(GeoLocation__c, GEOLOCATION(' + String.valueOf(sRLatitude) + ',' + String.valueOf(sRLongitude) + '),' + '\'mi' + '\') <' +limitRadius+') ';
					
				}
				
				if(serviceType.Duplicate_Threshold_Period__c!=null){
					//SR?s, Case.createddate >= Todays Date - Threshold Days.
					String sDTTM;
					DateTime DTTM = DateTime.newInstance(Date.today().addDays(-integer.valueof(serviceType.Duplicate_Threshold_Period__c)), Time.newInstance(0, 0, 0, 0));
					sDTTM = DTTM.formatGMT('yyyy-MM-dd') + 'T' + dttm.formatGMT('HH:mm:ss.SSS') + 'Z';
	                queryStr+='And CreatedDate >= '+ sDTTM;
	                
				}
				
				queryStr+=' order by CreatedDate DESC LIMIT 50';
				
				if(serviceType.DuplicateRadius__c==null && serviceType.Duplicate_Threshold_Period__c==null){
					return new list<case>();
				}
				
				system.debug('queryresults-->'+queryStr);
				serviceRequestList=database.query(queryStr);
			}
			catch (exception e){
				system.debug('unable to complete query process due-->'+e);
			}
			return serviceRequestList;
		}
		
		// building query string 
		private static string buildQueryRoot(){
			string query='Select c.Status,c.Service_Request_Number__c, c.CaseNumber, c.address__c,c.SRType__r.Duplicate_Threshold_Period__c, c.SRType__r.DuplicateRadius__c, c.SRType__r.Service_Name__c, c.SRType__c, c.IsClosed,'; 
			query+='c.GeoLocation__c, c.GeoLocation__Longitude__s, c.GeoLocation__Latitude__s, c.CreatedDate,c.SRType__r.DuplicateDetectionMethod__c,c.Service_Request_Id__c From Case c';
			return query;
		}
		
		// retrieve service request type based on service name
		private static ServiceRequestType__c getServiceType(String serviceName){
	        List<ServiceRequestType__c> srtypes= [Select Id,Name,Service_Name__c,Flex_Note_Count__c,External_Description__c,Flex_Notes_Link_Indicator__c,group__c,keywords__c,description__c,Agency__c,SLA__c,DuplicateDetectionMethod__c,Duplicate_Threshold_Period__c,DuplicateRadius__c FROM ServiceRequestType__c where Service_Name__c=:serviceName LIMIT 1];
	        system.debug('srtypes-->'+srtypes);
	        if(srtypes.isEmpty()){
	            //invalid service code provided data could not be found
	            throw new CustomException(APIConstants.NOT_FOUND);          
	        }
	        return srtypes[0]; 
	    }
    
        // wrapper class used for serialize and deserialize codedescription
        public class CodeDescriptionWrapper {
            string code;
            string description;
            
            public CodeDescriptionWrapper(string code, string description){
                this.code = code;
                this.description = description;
            }
        }                          
    
}