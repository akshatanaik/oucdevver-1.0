global class SnowAttributesFetchBatch implements Database.Batchable<sObject> ,Database.AllowsCallouts{
    
    
    public static boolean isSnowAttributeBatchRunning=false;
    
	global Database.QueryLocator start(Database.BatchableContext BC){
		
		string queryStr ='Select c.SRType__r.Agency__c, c.SRType__c, c.Jurisdiction__c, c.AgencyCode__c,c.YCOORD__c, c.XCOORD__c From Case c ';
		queryStr+=' where c.SRType__r.Agency__c = \''+string.escapesingleQuotes('SNOW')+'\'';
		return Database.getQueryLocator(queryStr);
	}
   
	global void execute(Database.BatchableContext BC, List<case> scope){
		// to avoid the future method to call'd from this batch class
		isSnowAttributeBatchRunning=true;
		
		// holds case to update with snow attributes
		list<case>caseListToUpdate =new list<case>();
		for(case caseRrd : scope){
			//case caseRrd = (case)s;
			
			if(caseRrd.XCOORD__c!=null && caseRrd.YCOORD__c!=null){
				
				String endUrlStr='http://maps2.dcgis.dc.gov/dcgis/rest/services/DCGIS_APPS/GeoAreas_311/MapServer/1/query';
				// geometry parameters
				endUrlStr+='?geometry = '+caseRrd.XCOORD__c+',+'+caseRrd.YCOORD__c;
				endUrlStr+='&geometryType=esriGeometryPoint&inSR=26985&spatialRel=esriSpatialRelIntersects&outFields=*&returnGeometry=false&f=pjson';
				
				string jsonResponse ;
				if(Test.isRunningTest()){
					jsonResponse ='{ "displayFieldName": "PARTIAL_AREA", "fieldAliases": { "OBJECTID": "OBJECTID", "PARTIAL_AREA": "PARTIAL_AREA", "SNOWAREA": "SNOWAREA", "SNOWZONE": "SNOWZONE", "SHAPE.AREA": "SHAPE.AREA", "SHAPE.LEN": "SHAPE.LEN" }, "fields": [ { "name": "OBJECTID", "type": "esriFieldTypeOID", "alias": "OBJECTID" }, { "name": "PARTIAL_AREA", "type": "esriFieldTypeSmallInteger", "alias": "PARTIAL_AREA" }, { "name": "SNOWAREA", "type": "esriFieldTypeSmallInteger", "alias": "SNOWAREA" }, { "name": "SNOWZONE", "type": "esriFieldTypeSmallInteger", "alias": "SNOWZONE" }, { "name": "SHAPE.AREA", "type": "esriFieldTypeDouble", "alias": "SHAPE.AREA" }, { "name": "SHAPE.LEN", "type": "esriFieldTypeDouble", "alias": "SHAPE.LEN" } ], "features": [ { "attributes": { "OBJECTID": 5, "PARTIAL_AREA": 10, "SNOWAREA": 308, "SNOWZONE": 3, "SHAPE.AREA": 0, "SHAPE.LEN": 0 } } ] }';
				}
				else{
					// Instantiate a new http object
		    		Http h = new Http();
					HttpRequest req = new HttpRequest(); 
					HttpResponse res = new HttpResponse();
					req.setEndpoint(endUrlStr);
					req.setMethod('GET');
					try{
						//Execute web service call here		
						res = h.send(req);	
						//Helpful debug messages
						System.debug(res.toString());
						System.debug('STATUS:'+res.getStatus());
						System.debug('STATUS_CODE:'+res.getStatusCode());
					  
					}catch(System.CalloutException e) {
						//Exception handling goes here....
						system.debug('unable to complete due to -'+e);
					}
					jsonResponse=res.getBody();
				}
				
				
				if(jsonResponse!=null){
					//string jsonstring= jsonResponse;
					string newJsonstring = jsonResponse.replace('"SHAPE.AREA":', '"AREA":'); 
					string newJsonstring1 = newJsonstring.replace('"SHAPE.LEN":', '"LEN":'); 
					JSONParser parser = JSON.createParser(newJsonstring1);
					snowAttributes.SnowAttributesWrapper snowAttritubeWrap =(SnowAttributes.SnowAttributesWrapper)parser.readValueAs(snowAttributes.SnowAttributesWrapper.class);

					if(snowAttritubeWrap.features!=null && snowAttritubeWrap.features.size()>=1){
						SnowAttributes.attributes att =snowAttritubeWrap.features[0].attributes;
						if(att!=null){
							if(att.SNOWAREA!=null)caseRrd.SnowRoute__c=string.valueof(att.SNOWAREA);
							if(att.SNOWZONE!=null)caseRrd.SnowZone__c =string.valueof(att.SNOWZONE);
							
							// collecting cases to update with snowzone and snow area values
							caseListToUpdate.add(caseRrd);
						}
					}
					
				}
			}
		}
		        
		if(!caseListToUpdate.isEmpty()) Update (caseListToUpdate);
	}

   global void finish(Database.BatchableContext BC){
   		
   }
   
   

   
}