public with sharing class ServiceRequestHistoryHelper {
	 /**
       * Queries for the specified request by service request id(case id) and returns the corresponding
       * response object.
       *
       * @param id  request's id
       * @return    A response version of the matching request.
       *            If no records are found, it will simply error message saying invalid/not found
       */
       public static Service_Request_History__c getRequest(String requestId){
            
            string query=buildGetQueryRoot();
            query+='WHERE CaseNumber__c=\'' +requestId +'\' ';

            List<Service_Request_History__c> cases = database.query(query);
                               
            if(cases.size()==0){
                 //invalid requestId code provided data could not be found
                  throw new CustomException(APIConstants.NOT_FOUND); 
            }
            return cases[0];
            
       }
       
       
        private static String buildGetQueryRoot(){
            string query = 'Select casenumber__c,status__c,description__c,Subject__c,Servicing_Agency__c,';
            query+='CreatedDate__c,LastModifiedDate,DateTimeOpened__c,LastUpdateDateTime__c,';
            query+='DateTimeClosed__c, Address__c,AddressID__c,geolocation__latitude__s,';
            query+='geolocation__longitude__s,media_url__c,Expected_Resolution_Date__c,ContactFirstName__c,Contactlastname__c,';
            query+='ContactPhone__c,ContactEmail__c,Priority__c,Origin__c,Device__c,SLA__c,External_Id__c,External_System_Name__c,Language__c,';
            query+='IntersectionId__c,Quadrant__c,Ward__c,District__c,PSA__c,ANC__c,SMD__c,Neighbor_Hood_Cluster__c,XCOORD__c,YCOORD__c,Zipcode__c,';
            query+='Jurisdiction_Code__c,IsClosedOnCreate__c,Reason__c,Service_Type_Code__c,';
            query+='Jurisdiction_Description__c,SRType__c,';
            query+='(Select  name,questions__c,Order__c, Answer__c, ';
            query+=' CodeDescription__c FROM Flex_Note_Histories__r ORDER BY Order__c, ID  ),';
            query+='(Select Id,Status__c,Task_Code__c,Task_Short_Name__c,Citizen_Email_On_Complete__c,';
            query+='Description__c,external_comments__c,outcome__c,Display_Sort_order__c,completion_date__c,';
            query+='Due_Date__c,Responsible_Party__c FROM OUC_Activity_Histories__r ORDER BY Display_Sort_Order__c )';
            query+='FROM Service_Request_History__c ';
            
            return query;
            
        }
       
        
     
       /**
       * Queries for list of Requests records and and returns the corresponding
       * response object (list of Requests).  If any non-null parameters are
       * provided, the list will be filtered by the specified field.
       *
       * @param jurisdiction_id    Id of jurisdiction
       * @return              Collection of Requests objects, as an list
       */
        public static List<Service_Request_History__c> listAllRequests(String filter){
           
            string query=buildGetQueryRoot();
            
            //adding filter
            if(filter==null){
                query +=' WHERE DateTimeOpened__c >= LAST_90_DAYS ';
            }
            
            if(filter!=null){
                query +=' WHERE '+filter;
            }
            
            // ER:  order by needs to by Last_Update_Date__c ASC when Update_Date is passed to GET SERVCIES
            if(filter.containsIgnoreCase('LastUpdateDateTime__c'))
                query+=' ORDER BY LastUpdateDateTime__c ASC ' ; // LIMIT 1000  (need to evaluate if we need LIMIT on this - LIKELY we do)
            else
                query+=' ORDER BY DateTimeOpened__c DESC LIMIT 1000 ';
            system.debug('FILTER:' + filter);
            return database.query(query);
            
        } 
}