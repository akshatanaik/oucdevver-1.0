public with sharing class APIValidations {
		
		/*
		  This class will contain all the validations that needs to be applied
		   ---- Add all the custom validations here ---
		*/
		
		
		public static Boolean isValidJuridiction(STring jId){
			//List<Jurisdiction__c> juri = [select id from Jurisdiction__c where Name=:jId];
			
			string validJurisdiction = 'dc.gov';
			if(jId.equalsIgnoreCase(validJurisdiction) || String.IsBlank(jId) ) return true;

			return false;			
		}
		
		public static APIKeyResponse validateKey(String key){
			
			List<Open311ApiKeyConfig__c> keyConfig =[Select Id,ApiKey__c,Private__c FROM Open311ApiKeyConfig__c WHERE ApiKey__c=:key];
			
			Open311ApiKeyConfig__c validKey;
			
			if(keyConfig.size()>0 ){
				for(Open311ApiKeyConfig__c k:keyConfig){
					if(key.equals(k.ApiKey__c)){
						validKey = k;
						break;
					}
				}	
			}
			
			APIKeyResponse response = new APIKeyResponse();
			response.isValid =false;
			response.apiKey=key;
			if(keyConfig.size()>0 && validKey!=null ){		
				response.isValid =true;	
				response.isPublic = !keyConfig[0].Private__c ;
				response.isPrivate=  keyConfig[0].Private__c ;
				
			}
			return response;
		}
		
		
		
		public class APIKeyResponse{
			public Boolean isValid {get;set;}
			public Boolean isPublic {get;set;}
			public Boolean isPrivate {get;set;}
			public String apiKey {get;set;}
			
			public APIKeyResponse(){
				isValid=false;isPublic=false;isPrivate=false;
			}
		}
		
		
}